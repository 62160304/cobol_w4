       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMIAPP.
       AUTHOR. SIRAPATSON.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  BMI   PIC 99v99 VALUE ZERO.
       01  DATA-MESSAGE   PIC X(35) VALUE SPACES.
           88 MS-UNDER    VALUE "Under Weight".
           88 MS-NORMAL   VALUE "Normal Weight".
           88 MS-OVER     VALUE "Over Weight".
           88 MS-OBESE    VALUE "Obese Weight".
           88 MS-EXTRE    VALUE "Extremely Weight".
       01  HEIGHT   PIC 999 VALUE ZERO.
       01  WEIGHT   PIC 999 VALUE ZERO.
       01  RESULT   PIC X(50).

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Enter height (cm) :- " WITH NO ADVANCING
           ACCEPT HEIGHT 
           DISPLAY "Enter weight (kg) :- " WITH NO ADVANCING
           ACCEPT WEIGHT 

           COMPUTE BMI = WEIGHT / ((HEIGHT/100)**2)
           DISPLAY "Bmi score is " BMI

      *    EVALUATE BMI
      *     WHEN 0     THRU 18.49  MOVE MS-UNDER   TO RESULT 
      *     WHEN 18.50 THRU 24.99  MOVE MS-NORMAL  TO RESULT 
      *     WHEN 25    THRU 29.99  MOVE MS-OVER    TO RESULT 
      *     WHEN 30    THRU 34.99  MOVE MS-OBESE   TO RESULT 
      *     WHEN 35    THRU 99     MOVE MS-EXTRE   TO RESULT   
      *    END-EVALUATE
      *    DISPLAY RESULT 
           .
